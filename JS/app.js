import axios from "axios";
import { User, displayUsers } from "./user";
import { createIcons } from "./icons";


const users = []

const userBox = document.getElementById('users');

document.body.addEventListener('click', function (e) {
    if (e.target.closest('#buttonAddUser') || e.target.closest('.adding-box')) {
        addUser();
    }
})

addUser();
addUser();
addUser();


function addUser() {
    axios.get("https://randomuser.me/api/")
        .then((response) => {
            const data = response.data.results[0];
            let picture = data.picture.large;
            let firstName = data.name.first;
            let lastName = data.name.last;
            let age = data.dob.age;
            let streetNumber = data.location.street.number;
            let streetName = data.location.street.name;
            let postCode = data.location.postcode;
            let city = data.location.city;
            let mail = data.email;

            const newUser = new User(picture, firstName, lastName, age, streetNumber, streetName, postCode, city, mail);
            users.push(newUser);


            userBox.innerHTML = displayUsers(users);
            createIcons();

        })
        .catch(error => {
            console.log(error);
        })
}






