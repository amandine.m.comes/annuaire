
export class User {
    constructor(picture, firstName, lastName, age, streetNumber, streetName, postCode, city, mail) {
        this.picture = picture;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.streetNumber = streetNumber;
        this.streetName = streetName;
        this.postCode = postCode;
        this.city = city;
        this.mail = mail;
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    get streetAdress() {
        return `${this.streetNumber} ${this.streetName}`;
    }

    get cityAdress() {
        return `${this.postCode} ${this.city}`;
    }


}

export function displayUsers(users) {
    let listOfUsers = ''


    users.forEach(user => {

        listOfUsers += `<div class="box"> 
        <div class="box-picture"> 
        <img  src= ${user.picture}  alt="profil picture"> 
        </div> 
        <div class="box-content"> 
        <li class="box-title"> ${user.fullName} </li> 
        <li class="content-items"> <i class ="icon" icon-name="gift"> </i> Age : ${user.age} </li> 
        <div class="content-items">
        <i class = "icon" icon-name="building"></i> 
        <div class="adress">
        <li> ${user.streetAdress} </li>
        <li> ${user.cityAdress} </li> 
        </div>
        </div>
        <li class="content-items"> <i class = "icon" icon-name="mail"></i> ${user.mail} </li> 
        </div> 
        </div>`;


    });

    return `${listOfUsers} <div class="adding-box">
    <i id = "userPlus" icon-name="user-plus"></i>
    </div>`;
}