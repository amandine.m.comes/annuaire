import { createIcons, UserPlus, Gift, Building, Mail } from "lucide";

export function createIcons() {
    createIcons({
        icons: {
            UserPlus,
            Gift,
            Building,
            Mail
        },
    });
}